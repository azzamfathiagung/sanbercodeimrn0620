console.log('LOOPING PERTAMA')
var flag = 2;
while(flag < 21) { // Loop akan terus berjalan selama nilai flag masih dibawah 10
  z = flag % 2;
  if (z == 0) {
    console.log(flag, '- I love coding'); // Menampilkan nilai flag pada iterasi tertentu
  }
  flag++; // Mengubah nilai flag dengan menambahkan 2
}


console.log('LOOPING KEDUA')
var flag = 20;
while(flag > 1) { // Loop akan terus berjalan selama nilai flag masih dibawah 10
    z = flag % 2;
    if (z == 0) {
  console.log(flag, '- I will become a mobile developer'); // Menampilkan nilai flag pada iterasi tertentu
    }
  flag--; // Mengubah nilai flag dengan menambahkan 2
}


console.log('For Loop')
for(var angka = 1; angka < 21; angka++) {
    var z = angka % 2;
    var x = angka % 3;
    if (z == 1) {
    console.log(angka, '- Santai');
    } if (z == 0) {
    console.log(angka, '- Berkualitas')
    } if (x == 0 && z == 1) {
    console.log(angka, '- I Love Coding')
    }
  } 


  console.log('Persegi Panjang')
  var flag = 1;
while(flag < 5) { // Loop akan terus berjalan selama nilai flag masih dibawah 10
  console.log('########'); // Menampilkan nilai flag pada iterasi tertentu
  flag++; // Mengubah nilai flag dengan menambahkan 1
}


console.log('Tangga')
var flag = 1;
var tangga = '';
while(flag < 8) { // Loop akan terus berjalan selama nilai flag masih dibawah 10
tangga = tangga + '#';
  console.log(tangga); // Menampilkan nilai flag pada iterasi tertentu
  flag++; // Mengubah nilai flag dengan menambahkan 1
}


console.log('Catur')
for(var angka = 1; angka < 9; angka++) {
    z = angka % 2;
    if (z == 1) {
    console.log(' # # # #');
    } else if (z == 0) {
    console.log('# # # # ')
    }
  } 
