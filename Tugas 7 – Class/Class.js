console.log('1. Animal Class')
console.log('Release 0')
class Animal {
    constructor(sheep) {
        this.name = sheep
    }
    get legs() {
        return this.name = 4
    }
    get cold_blooded() {
        return this.name = false
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log('')
console.log('Release 1')
class Ape {
    constructor(Ape) {
        this.name = Ape
    }
    get legs() {
        return this.name = 2
    }
    get yell() {
        return this.name = "Auooo"
    }
}

class model extends Ape {
    constructor(Ape, legs) {
        super(Ape);
        this.legs = legs
    }
}



class Frog {
    constructor(Frog) {
        this.name = Frog
    }
    get legs() {
        return this.name = 4
    }
    get jump() {
        return this.name = "hop hop"
    }
}

/*class model extends Frog {
    constructor(Frog, legs) {
        super(Frog);
        this.legs = legs
    }
}*/

var sungokong = new Ape("kera sakti")
console.log(sungokong.yell) // "Auooo"
 
var kodok = new Frog("buduk")
console.log(kodok.jump) // "hop hop" 

console.log('')
console.log('2. Function to Class')

function Clock({ template }) {
  
    var timer;
  
    function render() {
      var date = new Date();
  
      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      var output = template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    this.stop = function() {
      clearInterval(timer);
    };
  
    this.start = function() {
      render();
      timer = setInterval(render, 1000);
    };
  
  }
  
  var clock = new Clock({template: 'h:m:s'});
  clock.start(); 