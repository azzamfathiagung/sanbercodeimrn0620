var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
var index = 0
function start(times) {
    if (index < books.length) {
        readBooksPromise(times, books[index])
        .then(hasil => start(hasil))
        .catch(error => console.log(error))
    }
    index++
}

start(10000)